import socket
import struct
from time import sleep

from monitorcontrol import get_monitors

# aliases an constans
POWER_MODES = {
		"en":  1,
		"dis": 4,
}

current_monitors = [x for x in get_monitors()]


def change_lux(monitor, lux_meter, offset=44):
	with monitor:
		try:
			monitor.set_luminance(lux_meter + offset)
		except Exception as e:
			print(f"Cant change LUX! : {e}")


def change_power_mode(monitor, mode):
	with monitor:
		try:
			monitor.set_power_mode(mode)
		except Exception as e:
			print(f"CANT CHANGE POWER MODE!: {e}")
		return None


if __name__ == '__main__':
	
	# set defaults on socket
	MCAST_GRP = '224.1.1.1'  # receive ip
	MCAST_PORT = 54678  # receive port
	IS_ALL_GROUPS = True
	
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
	sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	
	# here set all bound ip's and multicast port (MCAST_GRP = 54679)
	if IS_ALL_GROUPS:
		# on this port, receives ALL multicast groups
		sock.bind(('', MCAST_PORT))
	else:
		# on this port, listen ONLY to MCAST_GRP
		sock.bind((MCAST_GRP, MCAST_PORT))
	
	#
	mreq = struct.pack("4sl", socket.inet_aton(MCAST_GRP), socket.INADDR_ANY)
	sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
	
	# main cycle
	while True:
		try:
			recv_pack = sock.recv(1024)  # lenth packet = 100
			lux_meter = recv_pack.decode(encoding="ansi")
			print(lux_meter)
			if lux_meter in POWER_MODES.keys():
				for monitor in current_monitors:
					change_power_mode(monitor, POWER_MODES[lux_meter])
			else:
				lux_meter = int(float(lux_meter[:-2]))
				print("CHANGE ", lux_meter)
				for monitor in current_monitors:
					change_lux(monitor, lux_meter)
		
		except TypeError:
			pass
