import socket
import struct
import re
import pyautogui # lib to use keys


keys = [
    'accept',
    'add',
    'alt',
    'altleft',
    'altright',
    'apps',
    'backspace',
    'browserback',
    'browserfavorites',
    'browserforward',
    'browserhome',
    'browserrefresh',
    'browsersearch',
    'browserstop',
    'capslock',
    'clear',
    'convert',
    'ctrl',
    'ctrlleft',
    'ctrlright',
    'decimal',
    'del',
    'delete',
    'divide',
    'down',
    'end',
    'enter',
    'esc',
    'escape',
    'execute',
    'f1',
    'f10',
    'f11',
    'f12',
    'f13',
    'f14',
    'f15',
    'f16',
    'f17',
    'f18',
    'f19',
    'f2',
    'f20',
    'f21',
    'f22',
    'f23',
    'f24',
    'f3',
    'f4',
    'f5',
    'f6',
    'f7',
    'f8',
    'f9',
    'final',
    'fn',
    'hanguel',
    'hangul',
    'hanja',
    'help',
    'home',
    'insert',
    'junja',
    'kana',
    'kanji',
    'launchapp1',
    'launchapp2',
    'launchmail',
    'launchmediaselect',
    'left',
    'modechange',
    'multiply',
    'nexttrack',
    'nonconvert',
    'num0',
    'num1',
    'num2',
    'num3',
    'num4',
    'num5',
    'num6',
    'num7',
    'num8',
    'num9',
    'numlock',
    'pagedown',
    'pageup',
    'pause',
    'pgdn',
    'pgup',
    'playpause',
    'prevtrack',
    'print',
    'printscreen',
    'prntscrn',
    'prtsc',
    'prtscr',
    'return',
    'right',
    'scrolllock',
    'select',
    'separator',
    'shift',
    'shiftleft',
    'shiftright',
    'sleep',
    'space',
    'stop',
    'subtract',
    'tab',
    'up',
    'volumedown',
    'volumemute',
    'volumeup',
    'win',
    'winleft',
    'winright',
    'yen',
    'command',
    'option',
    'optionleft',
    'optionright',
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'w',
    'x',
    'y',
    'z',
    '<',
    '=',
    '>',
]

# set defaults on socket
MCAST_GRP = '224.1.1.1'  # receive ip
MCAST_PORT = 54679		# receive port
IS_ALL_GROUPS = True

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# here set all bound ip's and multicast port (MCAST_GRP = 54679)
if IS_ALL_GROUPS:
    # on this port, receives ALL multicast groups
    sock.bind(('', MCAST_PORT))
else:
    # on this port, listen ONLY to MCAST_GRP
    sock.bind((MCAST_GRP, MCAST_PORT))

#
mreq = struct.pack("4sl", socket.inet_aton(MCAST_GRP), socket.INADDR_ANY)
sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)

# main cycle
while True:
    recv_pack = sock.recv(152)  # lenth packet = 152
    button_index = int(recv_pack.decode("utf-8"))

    if button_index == 151:
        pyautogui.hotkey(keys[102], keys[135]) # hotkey multibutton

    elif button_index == 152:
        pyautogui.hotkey(keys[102], keys[137]) # hotkey multibutton

    else:
        pyautogui.press(keys[button_index]) # 1 button pressed
