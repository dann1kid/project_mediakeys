# netifaces search broadcast
# make optimisation search local segmentation
# TODO: LAN area select
# TODO: iterate all interfaces

from netifaces import interfaces
from netifaces import ifaddresses

# interfaces of this machine
ifaces_list = []
for i in interfaces():
	ifaces_list.append(i)

# select first interface
interface_0 = ifaces_list[0]

# init list dicts of addresses (ip, netmask, mac, broadcast)
ls_int_0 = ifaddresses(interface_0)

# select keys of subaddresses
ls_adr_iface_0 = []
for i in ls_int_0.keys():
	ls_adr_iface_0.append(ls_int_0[i])

# select subaddresses with needed keys
for i in ls_adr_iface_0:
 	for j in (i[0].keys()):
 		if 'addr' and 'broadcast' in j:
 			print(f'{i[0]["addr"]} {i[0]["broadcast"]}')